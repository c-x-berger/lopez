from discord.ext import commands

import boiler


class moderate(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @boiler.bot_and_invoke_hasperms(manage_messages=True)
    @commands.hybrid_command(
        description="Enables a user with Manage Messages to bulk delete the last `amount` messages.",
        aliases=["delete"],
    )
    async def purge(self, ctx: commands.Context, amount: int):
        """Bulk remove messages."""
        channel = ctx.channel
        async with ctx.typing():
            deleted = await ctx.channel.purge(limit=amount + 1)
        em = boiler.embed_template(
            "Purged {} messages".format(len(deleted)), ctx.me.color
        )
        # We can't set *just* the footer text btw
        em.set_footer(
            text="Requested by {}".format(ctx.author.display_name),
            icon_url="https://i.imgur.com/2VepakW.png",
        )
        await channel.send(embed=em, delete_after=5 if len(deleted) <= 5 else None)


async def setup(bot):
    await bot.add_cog(moderate(bot))
