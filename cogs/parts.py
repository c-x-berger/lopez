import aiohttp
from bs4 import BeautifulSoup
from discord.ext import commands


class HTTPError(Exception):
    def __init__(self, response, message):
        self.response = response
        self.status = response.status
        if isinstance(message, dict):
            self.code = message.get("code", 0)
            base = message.get("message", "")
            errors = message.get("errors")
            if errors:
                # TODO: figure out what this should do
                # errors = flatten_error_dict(errors)
                helpful = "\n".join("In %s: %s" % t for t in errors.items())
                self.text = base + "\n" + helpful
            else:
                self.text = base
        else:
            self.text = message
            self.code = 0

        fmt = "{0.reason} (status code: {0.status})"
        if len(self.text):
            fmt = fmt + ": {1}"

        super().__init__(fmt.format(self.response, self.text))


class NotFound(HTTPError):
    pass


class InternalServerError(HTTPError):
    pass


class parts(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot

    async def cog_load(self):
        self.aio_client = aiohttp.ClientSession()

    @staticmethod
    def is_vexpro(num: str) -> bool:
        s = num.split("-")
        return (len(s) == 2) and (len(s[0]) == 3) and (len(s[1]) == 4)

    @staticmethod
    def mcmaster_part(part_num: str) -> str:
        return "https://www.mcmaster.com/{}".format(part_num.lower())

    @staticmethod
    def part_url(part_num: str) -> str:
        if parts.is_vexpro(part_num):
            return "https://www.vexrobotics.com/{}.html".format(part_num)
        elif "am-" in part_num:
            return "https://andymark.com/products/{}".format(part_num)

    async def get_part_page(self, url: str) -> BeautifulSoup:
        async with self.aio_client.get(url) as resp:
            if 300 > resp.status >= 200:
                data = await resp.text()
                return BeautifulSoup(data, features="html5lib")
            elif resp.status == 404:
                raise NotFound(resp, "Not found")
            else:
                raise InternalServerError(resp, "Server error at {}".format(url))

    @commands.hybrid_command(aliases=["parts"])
    async def part(self, ctx: commands.Context, part_numbers):
        """
        Look up a part by number/ID. Supports AndyMark and VexPro.
        """
        async with ctx.typing():
            r = {}
            part_numbers = [p.strip() for p in part_numbers.split(",")]
            for p in part_numbers:
                _p = None
                try:
                    _p = await self.get_part_page(parts.part_url(p))
                except (NotFound, InternalServerError):
                    r[p] = "Could not find part `{}`".format(p)
                else:
                    r[_p.title.contents[0]] = parts.part_url(p)
            for key, value in r.items():
                await ctx.send("{}: {}\n".format(key, value))

    @commands.hybrid_command(
        description="Looks up a part on McMaster Carr. Doesn't support part names or 404s.",
        aliases=["mc", "mmc", "mcmastercarr"],
    )
    async def mcmaster(self, ctx: commands.Context, part_numbers):
        """
        Looks up a part on McMaster Carr.
        """
        r = {}
        s = ""
        part_numbers = [p.trim_whitespace() for p in part_numbers.split(",")]
        for p in part_numbers:
            _p = None
            try:
                _p = await self.get_part_page(parts.mcmaster_part(p))
            except (NotFound, InternalServerError):
                r[p] = "Could not find part `{}`".format(p)
            else:
                r[_p.title.contents[0]] = self.mcmaster_part(p)
        for key, value in r.items():
            s += "{}: {}\n".format(key, value)
        await ctx.send(s)


async def setup(bot):
    await bot.add_cog(parts(bot))
