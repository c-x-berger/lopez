import asyncpg
import discord
from discord.ext import commands
from typing import List

import boiler


class roles(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def available_roles(self, guild_id: int) -> List[int]:
        return [
            r["role"]
            for r in await self.bot.connect_pool.fetch(
                "SELECT role FROM role_table WHERE guild = $1 AND status = 'available'",
                guild_id,
            )
        ]

    async def is_role_available(self, role_id: int, guild_id: int) -> bool:
        return await self.bot.connect_pool.fetchval(
            "SELECT EXISTS(SELECT 1 FROM role_table WHERE role = $1 AND guild = $2 AND status = 'available')",
            role_id,
            guild_id,
        )

    async def is_role_special(self, role_id: int, guild_id: int) -> bool:
        return await self.bot.connect_pool.fetchval(
            "SELECT EXISTS(SELECT 1 FROM role_table WHERE role = $1 AND guild = $2 AND status = 'special')",
            role_id,
            guild_id,
        )

    async def special_roles(self, guild_id: int) -> List[int]:
        return [
            r["role"]
            for r in await self.bot.connect_pool.fetch(
                "SELECT role FROM role_table WHERE guild = $1 AND status = 'special'",
                guild_id,
            )
        ]

    async def cog_check(self, ctx: commands.Context) -> bool:
        return boiler.guild_only_localcheck(ctx)

    @commands.hybrid_command(
        aliases=["add-giveme"],
        description="Adds a role to giveme. Any user will be able to give themselves the role with giveme <role>.",
    )
    @commands.has_permissions(manage_roles=True)
    async def add_giveme(self, ctx: commands.Context, *, role: discord.Role):
        """Adds a role to giveme"""
        await self.bot.connect_pool.execute(
            "INSERT INTO role_table(role, guild, status) VALUES ($1, $2, 'available') ON CONFLICT (role, guild) DO UPDATE SET status = 'available'",
            role.id,
            ctx.guild.id,
        )
        if not (ctx.guild.name.endswith("s") or ctx.guild.name.endswith("S")):
            await ctx.send(
                "Added {} to {}'s giveme roles.".format(role, ctx.guild.name)
            )
        else:
            await ctx.send("Added {} to {}' giveme roles.".format(role, ctx.guild.name))

    @commands.hybrid_command(
        aliases=["remove-giveme", "rm-giveme", "rm_giveme"],
        description="Removes a role from giveme. The role will show as not configured to users.",
    )
    @commands.has_permissions(manage_roles=True)
    async def remove_giveme(self, ctx: commands.Context, *, role: discord.Role):
        await self.bot.connect_pool.execute(
            "DELETE FROM role_table WHERE role = $1 AND guild = $2 AND status = 'available'",
            role.id,
            ctx.guild.id,
        )
        await ctx.send("Removed {} from giveme roles.".format(role.name))

    @commands.hybrid_command(
        aliases=["add-special", "special"],
        description='Blocks a role from giveme. "Special" roles explicitly cannot be user assigned or removed.',
    )
    @commands.has_permissions(manage_roles=True)
    async def add_special(self, ctx: commands.Context, *, role: discord.Role):
        """Blocks a role from giveme."""
        await self.bot.connect_pool.execute(
            "INSERT INTO role_table(role, guild, status) VALUES ($1, $2, 'special') ON CONFLICT (role, guild) DO UPDATE SET status = 'special'",
            role.id,
            ctx.guild.id,
        )
        if not (ctx.guild.name.endswith("s") or ctx.guild.name.endswith("S")):
            await ctx.send(
                "Blocked {} from {}'s giveme roles.".format(role.name, ctx.guild.name)
            )
        else:
            await ctx.send(
                "Blocked {} from {}' giveme roles.".format(role.name, ctx.guild.name)
            )

    @commands.hybrid_command(
        aliases=["unspecial", "remove-special"],
        description="Unblocks a role from giveme. The role will show as not configured to users.",
    )
    @commands.has_permissions(manage_roles=True)
    async def remove_special(self, ctx: commands.Context, *, role: discord.Role):
        """Unblocks a role from giveme."""
        await self.bot.connect_pool.execute(
            "DELETE FROM role_table WHERE role = $1 AND guild = $2 AND status = 'special'",
            role.id,
            ctx.guild.id,
        )
        await ctx.send("Unblocked {} from giveme roles.".format(role.name))

    @commands.hybrid_command()
    @discord.app_commands.guilds(286174293006745601)
    async def competition(self, ctx: commands.Context):
        """Gives the competition role. (3494 guild only.)"""
        if ctx.guild.id == 286174293006745601:
            await ctx.invoke(
                self.giveme,
                request=discord.utils.get(ctx.guild.roles, name="Competition"),
            )

    @commands.hybrid_command()
    @commands.bot_has_permissions(manage_roles=True)
    async def giveme(self, ctx: commands.Context, *, request: discord.Role):
        """Gives the requested role."""
        if await self.is_role_available(request.id, ctx.guild.id):
            await ctx.author.add_roles(request)
            await ctx.send(
                "Gave {} the {} role".format(ctx.author.mention, request.name)
            )
        elif await self.is_role_special(request.id, ctx.guild.id):
            await ctx.send(
                "You're not allowed to give yourself the {} role.".format(request.name)
            )
        elif request is not None:
            await ctx.send(
                "Could not find the role {} in any list for this guild! Please check for typos. If you need a list of available roles, do `[] listme`.".format(
                    request.name
                )
            )

    @commands.command()
    @boiler.bot_and_invoke_hasperms(manage_roles=True)
    async def assign(
        self, ctx: commands.Context, target: discord.Member, *roles: discord.Role
    ):
        """Forcibly assigns a role to a user."""
        roles = list(roles)
        for role in roles[:]:
            if role.position >= ctx.author.top_role.position:
                await ctx.send(
                    "You don't have the power to manage the `{}` role!".format(
                        role.name
                    )
                )
                roles.remove(role)
        if not roles:
            return
        await target.add_roles(*roles, reason="Requested by " + ctx.author.name)
        s_roles = ""
        for idx, role in enumerate(roles):
            if idx != len(roles) - 1 and idx != len(roles) - 2:
                s_roles += "`{}`, ".format(role.name)
            elif idx == len(roles) - 2:
                s_roles += "`{}`, and ".format(role.name)
            else:
                s_roles += "`{}`".format(role.name)
        await ctx.send("Gave {} the {} role(s)".format(target.mention, s_roles))

    @commands.command()
    @boiler.bot_and_invoke_hasperms(manage_roles=True)
    async def remove(
        self, ctx: commands.Context, target: discord.Member, *roles: discord.Role
    ):
        roles = list(roles)
        for role in roles[:]:
            if role.position >= ctx.author.top_role.position:
                await ctx.send(
                    "You don't have the power to manage the `{}` role!".format(
                        role.name
                    )
                )
                roles.remove(role)
        if not roles:
            return
        await target.remove_roles(*roles, reason="Requested by " + ctx.author.name)
        s_roles = ""
        for idx, role in enumerate(roles):
            if idx != len(roles) - 1 and idx != len(roles) - 2:
                s_roles += "`{}`, ".format(role.name)
            elif idx == len(roles) - 2:
                s_roles += "`{}`, and ".format(role.name)
            else:
                s_roles += "`{}`".format(role.name)
        await ctx.send("Took the {1} role(s) from {0}".format(target.mention, s_roles))

    @commands.hybrid_command(description="The opposite of giveme.")
    @commands.bot_has_permissions(manage_roles=True)
    async def removeme(self, ctx: commands.Context, *, request: discord.Role):
        """Removes the requested role."""
        if await self.is_role_available(request.id, ctx.guild.id):
            await ctx.author.remove_roles(request)
            await ctx.send(
                "Took the {1} role from {0}".format(ctx.author.mention, request.name)
            )
        elif await self.is_role_special(request.id, ctx.guild.id):
            await ctx.send(
                "You're not allowed to remove yourself from the {} role.".format(
                    request.name
                )
            )
        elif request is not None:
            await ctx.send(
                "Could not find the role {} in any list for this guild! Please check for typos. If you need a list of available roles, do `[] listme`.".format(
                    request.name
                )
            )

    @commands.hybrid_command(aliases=["rolelist"])
    async def listme(self, ctx: commands.Context):
        """Lists all roles available with giveme."""
        em = boiler.embed_template("List of Roles", ctx.me.color)
        em.description = "May not be all-encompassing. Only includes roles a guild moderator has set the status of."
        send = ""
        available = await self.available_roles(ctx.guild.id)
        for role in available:
            role = discord.utils.get(ctx.guild.roles, id=role)
            if role is not None:
                send += "* {}\n".format(role.name)
        if send != "":
            em.add_field(name="Available roles", value=send, inline=True)
        send = ""
        special = await self.special_roles(ctx.guild.id)
        for role_id in special:
            role = discord.utils.get(ctx.guild.roles, id=role_id)
            if role is not None:
                send += "* {}\n".format(role.name)
        if send != "":
            em.add_field(name="Roles blocked from giveme", value=send, inline=True)
        if len(em.fields) > 0:
            await ctx.send(embed=em)
        else:
            await ctx.send("No roles configured!")

    @commands.hybrid_command(aliases=["lusers", "rollcall"])
    async def listusers(self, ctx: commands.Context, *, role: discord.Role):
        """Get a list of users with the role `role`."""
        em = boiler.embed_template("Users with role {}".format(role.name), ctx.me.color)
        em.description = ""
        for m in role.members:
            em.description += m.mention + "\n"
        await ctx.send(embed=em)


async def setup(bot):
    await bot.add_cog(roles(bot))
