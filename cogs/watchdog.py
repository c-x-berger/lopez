import subprocess

from discord.ext import tasks, commands


class WatchdogCog(commands.Cog):
    def __init__(self):
        self.feeder.start()

    def cog_unload(self):
        self.feeder.cancel()

    @tasks.loop(seconds=15.0)
    async def feeder(self):
        subprocess.call(["/bin/systemd-notify", "WATCHDOG=1"])


async def setup(bot: commands.Bot):
    await bot.add_cog(WatchdogCog())
