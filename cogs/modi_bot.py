import logging

from discord.ext import commands


class ExtensionManager(commands.Cog):
    def __init__(self, bot: commands.Bot, special_cogs: list):
        self.bot = bot
        self.logger = logging.getLogger("cogs.ext_man")
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(self.bot.log_handler)
        self.special_cogs = special_cogs

    @commands.hybrid_command(name="tree-sync")
    @commands.is_owner()
    async def tree_sync(self, ctx: commands.Context):
        await self.bot.tree.sync()
        await ctx.send("ok done")

    @commands.hybrid_group(
        description="Base command for extension tinkering.\nCan only be invoked by the bot's creator.",
        case_insensitive=True,
        fallback="base",
    )
    @commands.is_owner()
    async def mod(self, ctx: commands.Context):
        """Base command for all extension tinkering."""
        if ctx.invoked_subcommand is None:
            await ctx.send(
                "This command must be invoked with a subcommand (`unload`, `load`, or `reload`)!"
            )

    @mod.command()
    async def load(self, ctx: commands.Context, module: str):
        """Load a module."""
        self.logger.info("Loading " + module + "...")
        if module not in self.special_cogs:
            await self.bot.load_extension(module)
            self.logger.info("Loaded " + module)
            await ctx.send("Loaded `{}`".format(module))

    @mod.command()
    async def unload(self, ctx: commands.Context, module: str):
        """Unload a module."""
        self.logger.info("Unloading " + module + "...")
        if module not in self.special_cogs:
            await self.bot.unload_extension(module)
            self.logger.info("Unloaded " + module)
            await ctx.send("Unloaded `{}`".format(module))

    @mod.command(description="Reload a module.")
    async def reload(self, ctx: commands.Context, module: str):
        """Reload a module."""
        try:
            await self.bot.reload_extension(module)
        except commands.ExtensionError as e:
            await ctx.send("Could not reload `{}`: {}".format(module, str(e)))
        else:
            await ctx.send("Reloaded `{}`".format(module))


async def setup(bot: commands.Bot):
    await bot.add_cog(ExtensionManager(bot, ["main", "cogs.modi_bot"]))
