Lopez
---
<img src="https://gitlab.com/c-x-berger/lopez/-/raw/master/img/lopez.png" alt="Lopez's profile picture" align="right" width="400px">  
<a href="https://github.com/ambv/black"><img src="https://img.shields.io/badge/code%20style-black-000000.svg" alt="Code style: black"></a>

🐍 🤖

Lopez is a Discord bot written in Python (3) with the help of [`discord.py`](https://github.com/Rapptz/discord.py).  
It manages a few irritating admin tasks for Discord administration automatically (such as self-assigned roles). It's also a budding DnD fan.

## Requirements
* Python 3.8 or better
* `discord.py` v1.0.0 (latest is greatest, unless the latest is breaking)
* `asyncpg` and dependencies

## Installation
[Use this link to invite Lopez into your Discord server!][oauth-add-lopez]

(instructions on starting a dev instance can be found in [`INSTALL.md`](https://gitlab.com/c-x-berger/lopez/-/blob/develop/INSTALL.md#developing))

# Copyright Notices
Copyright 2020 Caleb Xavier Berger

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this program except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Prior to May 2019, this project was owned by The Quadrangles FRC 3494, and available under the MIT License. (The current ownership better reflects the project's creator, maintainer, and host.)

Lopez's avatar (pictured above) is (C) FIRST 2018, and is thought to be fair use.

[oauth-add-lopez]: https://discordapp.com/oauth2/authorize?client_id=436251140376494080&scope=bot&permissions=268822737