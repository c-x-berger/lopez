# Installation
This document aims to be a step-by-step guide to installing Lopez.  
It ~~probably~~ fails in this.

## For all purposes
1. Install and configure `postgresql`
    - You must have at least one database with at least one user who can create and modify tables.
2. Install Lopez's Python module requirements with `pipenv install`.
3. Run `db_setup.py` to set up Postgres tables. Note that `db_setup.py` makes **absolutely no attempt** to upgrade an existing install.
4. Create `creds.txt`. It should contain only the Discord bot token you want to use to log in on a single line.
5. Create the `logs` directory for - you guessed it - logs.

### Configuration
To configure how Lopez talks to to Postgres, the following environment variables can be used:
* `POSTGRES_ROLE`: The "user" Lopez uses when communicating with Postgres. Defaults to `lopez`.
* `POSTGRES_PASSWORD`: The password used to authenticate with Postgres. Defaults to `johny johny telling lies`.
* `POSTGRES_DB`: The database Lopez will use. Defaults to `lopez`.
* `POSTGRES_HOST`: The server hosting Postgres. Defaults to `localhost`.
* `POSTGRES_PORT`: The port Postgres is running on. Defaults to `5432`. Must be an integer or unset.

In addition, Lopez has limited interactions with systemd:
* `LOPEZ_SYS_D_MON`: If set to `1`, Lopez will call `/bin/systemd-notify` about every 15 seconds for systemd process management.

## Developing
1. `git checkout develop && git checkout -b my-super-dank-feature`
2. Install dependencies with `pipenv install --dev`. This will pull dependencies required for Lopez to run, as well as development tools like `black`.
3. Write code. Send it back as a merge request. Get a sense of pride and accomplishment.
4. Be sure to check [the most recent revision of this file](https://gitlab.com/c-x-berger/lopez/-/blob/develop/INSTALL.md) in the unlikely event that the requirements change. 

## Running
[Last chance to use the stable, well serviced public instance instead!][oauth-add-lopez]
### With Docker
Simply `docker build -t whatever .` and then run the built image. The image exposes no services and needs no volumes to be mounted, though networking it to connect to Postgres is an excercise left to the end user.
(Have you heard of our lord and savior Kubernetes?)

### Manually
`pipenv run --three python main.py` should start Lopez without issue.

[oauth-add-lopez]: https://discordapp.com/oauth2/authorize?client_id=436251140376494080&scope=bot&permissions=268822737