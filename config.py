import os


def env_or_default(key: str, default: str) -> str:
    try:
        return os.environ[key]
    except KeyError:
        return default


sql_user = env_or_default("POSTGRES_ROLE", "lopez")
# for obvious reasons this should be changed
sql_pass = env_or_default("POSTGRES_PASSWORD", "johny johny telling lies")
sql_db = env_or_default("POSTGRES_DB", "lopez")
sql_host = env_or_default("POSTGRES_HOST", "localhost")
sql_port = int(env_or_default("POSTGRES_PORT", "5432"))
# changing the below line will void the non-existent warranty Lopez came with
# so please don't
postgresql = "postgresql://{0}:{1}@{2}:{3}/{4}".format(
    sql_user, sql_pass, sql_host, sql_port, sql_db
)

# SystemD process monitoring garbage
systemd = int(env_or_default("LOPEZ_SYS_D_MON", "0")) == 1
