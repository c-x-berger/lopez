FROM python:3.9

WORKDIR /usr/src/lopez

# Set up dependencies
COPY Pipfile ./
COPY Pipfile.lock ./
RUN pip install --no-cache-dir pipenv
RUN pipenv install

# You *did* bring your credentials, right?
COPY creds.txt ./
# Copy the rest of Lopez into the container
COPY . .
# lol
RUN mkdir ./logs

ENTRYPOINT ["pipenv", "run", "--three", "python", "main.py"]