__license__ = "MIT"
__author__ = "Caleb Xavier Berger"
__copyright__ = "(c) 2020 Caleb Xavier Berger"
__copynote__ = (
    "originally property of the Quadrangles, turned over to present owner upon request"
)
